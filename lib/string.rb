class String
  def to_number_of_seconds
    sec_per_interval = { yea: 31_536_000, mon: 2_592_000, day: 86_400, hou: 3_600, min: 60, sec: 1 }
    scan(/(\d+)\s(\w{3})/).inject(0) { |sum, val| sum + (val[0].to_i * sec_per_interval[val[1].to_sym]) }
  end
end
