require 'rspec'
require_relative '../lib/string'
RSpec.describe String do
  context 'to correct convert_to_sec' do
    it '59 seconds to second' do
      input = '59 seconds'
      expect(input.to_number_of_seconds).to eq 59
    end
    it '1 year and 59 seconds to second' do
      input = '1 year 59 seconds'
      expect(input.to_number_of_seconds).to eq 31_536_059
    end
    it '1 year' do
      input = '1 year'
      expect(input.to_number_of_seconds).to eq 31_536_000
    end
  end
end
